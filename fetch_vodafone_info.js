const md5 = require('md5');
const qs = require('querystring');
const axios = require('axios');
const SocksProxyAgent = require('socks-proxy-agent');
let USE_PROXY = false;
let proxies = require('./proxylist').proxies;
const REQUEST_URL = require('./templates/vodafonerequestbody').REQUEST_URL;
const REQUEST_BODY = require('./templates/vodafonerequestbody').REQUEST_BODY;
const REQUEST_TIMEOUT = 60000;

const config = {
    headers: {
        'accept': 'application/json, text/plain, */*',
        'authorization': 'Bearer fc51193c-a637-35ba-8dd6-2550f186eed6',
        'channel': 'VF-CON-APP',
        'if-none-match': '',
        'connection': 'close',
        'x-businesstx-id': 'B609658733',
        'x-external-correlationid': 'C609658747',
        'Content-Type': 'application/json',
        'Accept-Encoding': 'gzip',
        'User-Agent': 'okhttp/3.12.12'
    },
    timeout: REQUEST_TIMEOUT
}

module.exports.fetchVodafoneInfo = async function (tel_number) {
    tel_number = tel_number.substring(4);
    const requestBody = Object.assign({}, REQUEST_BODY);
    requestBody.msisdn = tel_number;
    requestBody.parentMSISDN = tel_number;
    let httpsAgent = new SocksProxyAgent(proxies[Math.floor(Math.random() * proxies.length)]);
    let httpAgent = httpsAgent;
    let client = axios;
    if (USE_PROXY) {
        client = axios.create({ httpsAgent, httpAgent })
    }
    let result = await client.post(REQUEST_URL, requestBody, config);
    console.log("processed:", tel_number);
    return result.data;
}