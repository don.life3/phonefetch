const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', { useNewUrlParser: true });

let phones = require("./phones").phones;
let ranges = [
    //Airtel
    '99100-99109', '98100-98109',
    '98180-98189', '98710-98719',
    '99580-99589', '97170-97179',
    '95600-95609', '96500-96509',
    '99710-99719', '88000-88009',
    '88260-88269', '85270-85279',
    '81300-81309', '91330-91339',
    '95990-95999', '98210-98219',
    '92050-92059', '97735-97739',
    '98701-98705', '84480-84489',
    '96670-96679', '93550-93559',
    '73030-73039', '93110-93119',
    '74280-74289',
    //Vodafone
    '98730-98739', '98990-98999', 
    '98110-98119', '99530-99539',
    '95820-95829', '96540-96549', 
    '97110-97119', '99990-99999', 
    '78380-78389', '88600-88609',
    '84470-84479', '83779', '83778', 
    '83768-83770', '83758-83760',
    '83750', '83739', '96430-96439',
    '72890', '72898-72900', '72908-72910',
    '72918-72920', '76690-76699',
    //Rcom
    '90150-90159', '80100-80109', 
    '78270-78279', '74280-74289', 
    '88820-88829', '85950-85959',
    '82870-82879', '84710', '84709', 
    '84708', '84700', '84689', '84688',
    '84680', '84678', '84670', '78598-78599',
    '78610', '78618-78620', '78628-78630', '78638',
    '70110-70119',
    //Idea
    '99110-99114', '98910-98919', 
    '97180-97189', '99900-99909', 
    '87500-87509', '87459', 
    '87448', '87458', '87449', '87450', 
    '87438', '87439', '87429', '87430', 
    '85058-85060', '85068', '85069', 
    '85100', '85108', '85109', '85120',
    '85128', '78348-78350', '78358-78360', 
    '78368-78369', '78400', '78408', '93410-93419',
    //Reliance Jio
    '79820-79829', '80760-80769', 
    '81780-81789', '83680-83689', 
    '88510-88519', '89200-89209', 
    '83830', '83838-83840', '76781-76786', 
    '93540-93549', '93100-93109', '78270-78279', 
    '82870-82879', '85950-85959', '88820-88829',
    //Aircel
    '97160-97169', '88020-88029', 
    '75030-75039', '82850-82859', 
    '75328-75330', '75318-75320', 
    '75308-75310', '75299',
    //MTNL
    '99680-99689', '98680-98689', 
    '90130-90139',
    //Loop
    '91030-91039',
    //Datacom
    '90660-90669',
    //Etisalat
    '95990-95999',
    //Shyam Telelink
    '91360-91369', '84590-84599',
    //Spice
    '91550-91559',
    //Unitech wireless
    '90840-90849'   
];
let endSequence = "93";


const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
    ranges.forEach(async (range) => {
        let r = range.split("-");
        let br = Number(r[0]), er = Number(r[1]);
        if (!er) er = br;
        for (let i = br; i <= er; i++) {
            for (let j = 0; j <= 999; j++) {
                let phoneNumber = '+91-' + i + pad(j,3) + endSequence;
                await insertPhoneNumber(phoneNumber);
            }
        }
    });
});

let insertPhoneNumber = (phoneNumber) => {
    return new Promise((resolve, reject) => {
        let phoneInfo = {
            phoneNumber: phoneNumber
        };

        phones.create(phoneInfo, (err, result) => {
            if (err) {
                // console.log(err);
                reject(err);
            } else {
                // console.log(result);
                resolve(result);
            }
        });
    });
};

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
  }