const SocksProxyAgent = require('socks-proxy-agent');
const querystring = require('querystring');
const mustache = require('mustache');
var FormData = require('form-data');
let count = 0;
let DEBUG_MODE = false;
let USE_PROXY = false;
let REQUEST_TIMEOUT = 90000;
let axios = require('axios');

const { REQUEST_TYPES } = require('./providers');
if (DEBUG_MODE) {
    process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
    axios = require('axios-https-proxy-fix');
}
const DEFAULT_HEADERS = {
    "user-agent": "Mozilla/5.0 (Windows NT 8.0; Win32; x32; rv:58.0) Gecko/20100101 Firefox/57.0"
};
let pool = [];

let proxies = require('./proxylist').proxies;

const requestMethods = require("./providers").REQUEST_METHODS;
const requestTypes = require("./providers").REQUEST_TYPES;
let providers = require("./providers").PROVIDERS;

function fetchRequest(option, phoneNumber) {
    let reqUrl = providers[option].reqUrl;
    let reqHeaders = providers[option].reqHeaders || null;
    let reqData = providers[option].reqData || null;
    let reqType = providers[option].reqType || null;
    let reqMethod = providers[option].reqMethod;

    let values = {
        countryCode: "91",
        phoneNumber: phoneNumber,
        sequence: phoneNumber.substring(0, 7)
    }

    if (reqUrl) {
        if (typeof reqUrl == 'string') {
            reqUrl = mustache.render(reqUrl, values);
        } else {
            reqUrl = JSON.parse(mustache.render(JSON.stringify(reqUrl), values));
        }
    }

    if (reqData) {
        if (typeof reqData == 'string') {
            reqData = mustache.render(reqData, values);
        } else {
            if (reqType == REQUEST_TYPES.MULTIPART) {
                reqData = JSON.parse(mustache.render(JSON.stringify(reqData), values));
                let formData = new FormData();
                Object.keys(reqData).forEach((key) => {
                    formData.append(key, reqData[key])
                })
                reqData = formData;
            } else {
                reqData = JSON.parse(mustache.render(JSON.stringify(reqData), values));
            }
        }
    }

    if (reqHeaders) {
        if (typeof reqHeaders == 'string') {
            reqHeaders = mustache.render(reqHeaders, values);
        } else {
            reqHeaders = JSON.parse(mustache.render(JSON.stringify(reqHeaders), values))
        }
    }

    let client = axios;
    let proxy = null;
    if (USE_PROXY && DEBUG_MODE) {
        proxy = {
            host: "localhost",
            port: "8866"
        }
    } else {
        if (USE_PROXY) {
            let httpsAgent = new SocksProxyAgent(proxies[Math.floor(Math.random() * proxies.length)])
            let httpAgent = httpsAgent

            client = axios.create({ httpsAgent, httpAgent })
        } else {
            client = axios
        }
    }
    if (!reqHeaders) reqHeaders = DEFAULT_HEADERS;
    if (reqMethod == requestMethods.GET) {
        return client.get(reqUrl, {
            headers: reqHeaders,
            params: reqData,
            proxy: proxy,
            timeout: REQUEST_TIMEOUT
        })
    } else if (reqMethod == requestMethods.POST) {
        reqHeaders['Content-Type'] = reqType;
        if (reqType == REQUEST_TYPES.MULTIPART) {
            reqHeaders['Content-Type'] = reqType + "; boundary=" + reqData._boundary;
        }
        if (reqType == requestTypes.FORM) {
            return client.post(reqUrl, querystring.stringify(reqData), {
                headers: reqHeaders,
                proxy: proxy,
                timeout: REQUEST_TIMEOUT
            })
        } else {
            return client.post(reqUrl, reqData, {
                headers: reqHeaders,
                proxy: proxy,
                timeout: REQUEST_TIMEOUT
            })
        }
    } else if (reqMethod == requestMethods.PUT) {
        reqHeaders['Content-Type'] = reqType;
        if (reqType == requestTypes.FORM) {
            return client.put(reqUrl, querystring.stringify(reqData), {
                headers: reqHeaders,
                proxy: proxy,
                timeout: REQUEST_TIMEOUT
            })
        } else {
            return client.put(reqUrl, reqData, {
                headers: reqHeaders,
                proxy: proxy,
                timeout: REQUEST_TIMEOUT
            })
        }
    }
}


function refreshPool(phNo) {
    pool = [];
    
    pool.push(new Promise((resolve, reject) => {
        fetchRequest("123vahak", phNo).then((success) => {
            resolve({
                provider: "123vahak",
                status: success.status,
                statusText: success.statusText
            });
        }).catch((err) => {
            resolve(err);
        })
    }));

    return pool;
}

let nCount = 0;
module.exports.startOperations = function(phNo) {
    return new Promise((resolve, reject)=>{
        Promise.all(refreshPool(phNo)).then((results) => {
            console.log(results);
            count = count + Object.keys(providers).length;
            process.stdout.write("Done:" + nCount + " \r");
            nCount = nCount+1;
            resolve();
        }).catch((err) => {
            console.log(err);
            reject();
        });
    })
    
}
