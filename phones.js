const mongoose = require('mongoose');

const phoneSchema = new mongoose.Schema({
    phoneNumber: { type: String, required: true, index: true, unique: true },
    name: { type: String },
    name_tags: [String],
    type: { type: String },
    operator: { type: String },
    belong_area: { type: String },
    processed: {
        type: Boolean,
        default: false,
        required: true
    },
    operator_processed: {
        type: Boolean,
        default: false,
        required: true
    },
    sms_sent:{
        type: Boolean,
        default: false,
        required: true
    },
    eyecon_processed:{
        type: Boolean,
        default: false,
        required: true
    },
    customerId: { type: String },
    circleId: { type: String },
    segment: { type: String },
    status: { type: String },
    "isMigrated": { type: String },
    "activationDate": { type: String },
    "subscriptionType": { type: String },
    "eyecon_name": { type: String }
});

let phones = mongoose.model('PhoneNumbers', phoneSchema);
// phones.createIndexes();

module.exports.phones = phones;
