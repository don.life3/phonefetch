const md5 = require('md5');
const qs = require('querystring');
const axios = require('axios');
const SocksProxyAgent = require('socks-proxy-agent');
let USE_PROXY = false;
let proxies = require('./proxylist').proxies;
const SEARCH_URL = require('./templates/searchrequestbody').SEARCH_URL;
const TIME_URL = require('./templates/timerequestbody').TIME_URL;
const timeRequestBody = require('./templates/timerequestbody').TIME_REQUEST_BODY;
const REQUEST_TIMEOUT = 60000;

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; Google Nexus 5X Build/MRA58K)',
        'Accept-Encoding': 'gzip'
    },
    timeout: REQUEST_TIMEOUT
}

let time_data = null;

module.exports.getTimeData = async () => {
    try {
        let result = await axios.post(TIME_URL, qs.stringify(timeRequestBody), config);
        if (result && result.data) {
            time_data = JSON.parse(happyBase64Decode(result.data));
            time_data.timestamp = (parseInt(Date.now() / 1000)) - time_data.timestamp;
        }
    } catch (err) {
        console.log(err);
    }
}

module.exports.fetchPhoneInfo = async function (tel_number) {
    const searchRequestBody = Object.assign({}, require('./templates/searchrequestbody').SEARCH_REQUEST_BODY);
    searchRequestBody.tel_number = tel_number;
    if (time_data && time_data.timestamp != null) {
        let stamp = (parseInt(Date.now() / 1000) - time_data.timestamp).toString();
        let ph_no = tel_number;
        searchRequestBody.stamp = getStamp(stamp, ph_no);
        let httpsAgent = new SocksProxyAgent(proxies[Math.floor(Math.random() * proxies.length)])
        let httpAgent = httpsAgent
        let client = axios;
        if(USE_PROXY){
            client = axios.create({ httpsAgent, httpAgent })
        }
        let result = await client.post(SEARCH_URL, qs.stringify(searchRequestBody), config);
        console.log("processed:", ph_no);
        return happyBase64Decode(result.data);
    }
}

function getStamp(stamp, ph_no) {
    let sb = "";
    sb = ph_no + stamp + "this_is_mobile_2014";
    return md5(sb);
}

function happyBase64Decode(str) {
    let i;
    let sb = "";
    for (let i2 = 0; i2 < str.length; i2++) {
        let charAt = str[i2];
        if ((charAt < '0' || charAt > '9') && ((charAt < 'A' || charAt > 'Z') && (charAt < 'a' || charAt > 'z'))) {
            i = charAt.charCodeAt(0);
        } else {
            i = charAt.charCodeAt(0) - (i2 % 10);
            if (i < 48) {
                i = (122 - (48 - i)) + 1;
            } else if (charAt >= 'A' && i < 65) {
                i = (57 - (65 - i)) + 1;
            } else if (charAt >= 'a' && i < 97) {
                i = (90 - (97 - i)) + 1;
            }
        }
        try {
            sb = sb + String.fromCharCode(i);
        } catch (e) {
            e.printStackTrace();
        }
    }
    try {
        return Buffer.from(sb, 'base64').toString('utf-8');
    } catch (e2) {
        console.log(e2);
        return "";
    }
}