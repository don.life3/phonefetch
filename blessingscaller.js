const mongoose = require('mongoose');
const blessings = require('./blessings');
mongoose.connect('mongodb+srv://testuser:admin@cluster0.etnk8.mongodb.net/test', { useNewUrlParser: true });

let phones = require("./phones").phones;

async function parsePhoneRecord() {

    try {
        let record = await phones.findOne({
            $and: [
                {
                    name: {
                        "$exists" : true, "$ne" : ""
                    }
                },
                {
                    processed: {
                        "$exists" : true, "$ne" : false
                    }
                },
                {
                    sms_sent: {
                        "$exists" : true, "$ne" : true
                    }
                }
            ]

        }).exec();
        if(!record || !record._doc){
            console.log("Finished processing");
            return;
        }
        console.log(record._doc.phoneNumber);
        let results = await blessings.startOperations(record._doc.phoneNumber);
        updateCallerInfo(record._doc._id);
    } catch (err) {
        console.log(err);
    }

}

let updateCallerInfo = (id)=>{
    let updateData = {
        sms_sent: true
    };
    phones.findByIdAndUpdate(id, updateData,(err, res)=>{
        if(err) console.log(err);
        // else if(res) console.log(res);
        setTimeout(() => {
            parsePhoneRecord();
        }, 30000);
    })
}


parsePhoneRecord();
