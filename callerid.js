const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://testuser:admin@cluster0.etnk8.mongodb.net/test', { useNewUrlParser: true });

let phones = require("./phones").phones;
let fetchPhoneInfo = require("./fetch_phone_info");
fetchPhoneInfo.getTimeData();

async function parsePhoneRecord() {

    try {
        let record = await phones.findOne({
            $or: [
                {
                    processed: {
                        $exists: false
                    }
                },
                {
                    processed: {
                        $eq: false
                    }
                }
            ]

        }).exec();
        if(!record || !record._doc){
            console.log("Finished processing");
            return;
        }
        // console.log(record._doc.phoneNumber);
        let callerInfo = await fetchPhoneInfo.fetchPhoneInfo(record._doc.phoneNumber);
        updateCallerInfo(record._doc._id, JSON.parse(callerInfo));
    } catch (err) {
        console.log(err);
        setTimeout(() => {
            parsePhoneRecord();
        }, 1000);
    }

}

let updateCallerInfo = (id, callerInfo)=>{
    let updateData = {
        processed: true
    };
    if(callerInfo){
        if(callerInfo.name){
            updateData.name = callerInfo.name;
        }
        if(callerInfo.operator){
            updateData.operator = callerInfo.operator;
        }
        if(callerInfo.type){
            updateData.type = callerInfo.type;
        }
        if(callerInfo.belong_area){
            updateData.belong_area = callerInfo.belong_area;
        }
        if(callerInfo.name_tags && typeof callerInfo.name_tags == "object" && Array.isArray(callerInfo.name_tags)){
            updateData.name_tags = callerInfo.name_tags;
        }
    }

    phones.findByIdAndUpdate(id, updateData,(err, res)=>{
        if(err) console.log(err);
        // else if(res) console.log(res);
        setTimeout(() => {
            parsePhoneRecord();
        }, 1000);
    })
}


parsePhoneRecord();
