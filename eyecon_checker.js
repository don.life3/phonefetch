const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });

let phones = require("./phones").phones;
let fetchEyeconInfo = require("./fetch_eyecon_info");

async function parsePhoneRecord() {
    let recordId = null;
    try {
        let record = await phones.findOne({
            $and: [
                {
                    processed: true
                },
                {
                    $or:[
                        {"eyecon_processed": false},
                        {"eyecon_processed":{$exists: false}}
                    ]
                },
                {
                    $or: [
                        { name: { $regex: 'shyam', $options: 'i' } },
                        { name: { $regex: 'sagar', $options: 'i' } },
                        { name: { $regex: 'shayam', $options: 'i' } },
                        { name: { $regex: 'negi', $options: 'i' } }
                    ]

                }
            ]

        }).exec();
        if (!record || !record._doc) {
            console.log("Finished processing");
            return;
        }
        recordId = record._doc._id;
        let callerInfo = await fetchEyeconInfo.fetchEyeconInfo(record._doc.phoneNumber);
        updateCallerInfo(record._doc._id, callerInfo);
    } catch (err) {
        console.log(err);
        updateCallerInfo(recordId, null);
    }

}

let updateCallerInfo = (id, callerInfo) => {
    let updateData = {
        eyecon_processed: true
    };
    if (callerInfo && Array.isArray(callerInfo) && callerInfo[0]) {
        updateData.eyecon_name = callerInfo[0].name;
    }

    phones.findByIdAndUpdate(id, updateData, (err, res) => {
        if (err) console.log(err);
        // else if(res) console.log(res);
        setTimeout(() => {
            parsePhoneRecord();
        }, 10000);
    })
}


parsePhoneRecord();
