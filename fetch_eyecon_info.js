const md5 = require('md5');
const qs = require('querystring');
const SocksProxyAgent = require('socks-proxy-agent');
let USE_PROXY = true;
let proxies = require('./proxylist').proxies;
const REQUEST_URL = require('./templates/eyeconrequestbody').REQUEST_URL;
const REQUEST_BODY = require('./templates/eyeconrequestbody').REQUEST_BODY;
const REQUEST_TIMEOUT = 60000;

const config = {
    headers: {
        'accept': 'application/json',
        'e-auth-v': 'e1',
        'e-auth': '1e5ed8a2-b9ee-4dce-81c4-2db0708a4e52',
        'e-auth-c': '26',
        'accept-charset': 'UTF-8',
        'x-businesstx-id': 'B609658733',
        'x-external-correlationid': 'C609658747',
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        'Accept-Encoding': 'gzip, deflate',
        "Connection": "Keep-Alive",
        "Host": "api.eyecon-app.com",
        'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; Google Build/MRA58K)'
    },
    timeout: REQUEST_TIMEOUT
}

module.exports.fetchEyeconInfo = async function (tel_number) {
    const axios = require('axios');
    tel_number = tel_number.substring(4);
    let requestBody = Object.assign({}, REQUEST_BODY);
    requestBody.cli = "91"+tel_number;
    let httpsAgent = new SocksProxyAgent(proxies[Math.floor(Math.random() * proxies.length)]);
    let httpAgent = httpsAgent;
    let client = axios.create();
    if (USE_PROXY) {
        client = axios.create({ httpsAgent, httpAgent })
    }
    let result = await client.get(REQUEST_URL,{
        headers: config.headers,
        params: requestBody,
        timeout: REQUEST_TIMEOUT
    });
    console.log("processed:", tel_number);
    console.log(result.data);
    return result.data;
}