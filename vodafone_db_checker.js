const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });

let phones = require("./phones").phones;
let fetchVodafoneInfo = require("./fetch_vodafone_info");

async function parsePhoneRecord() {
    let recordId = null;
    try {
        let record = await phones.findOne({
            $and: [
                {
                    processed: true
                },
                {
                    $or:[
                        {"operator_processed": false},
                        {"operator_processed":{$exists: false}}
                    ]
                },
                {
                    operator:{$regex:'vodafone', $options:'i'}
                }
            ]

        }).exec();
        if(!record || !record._doc){
            console.log("Finished processing");
            return;
        }
        recordId = record._doc._id;
        let callerInfo = await fetchVodafoneInfo.fetchVodafoneInfo(record._doc.phoneNumber);
        updateCallerInfo(record._doc._id, callerInfo);
    } catch (err) {
        console.log(err);
        updateCallerInfo(recordId, {});
    }

}

let updateCallerInfo = (id, callerInfo)=>{
    let updateData = {
        operator_processed: true
    };
    if(callerInfo && callerInfo.customer){
        if(callerInfo.customer.id){
            updateData.customerId = callerInfo.customer.id;
        }
        if(callerInfo.customer.circleId){
            updateData.circleId = callerInfo.customer.circleId;
        }
        if(callerInfo.customer.segment){
            updateData.segment = callerInfo.customer.segment;
        }
        if(callerInfo.customer.status){
            updateData.status = callerInfo.customer.status;
        }
        if(callerInfo.customer.isMigrated){
            updateData.isMigrated = callerInfo.customer.isMigrated;
        }
        if(callerInfo.customer.activationDate){
            updateData.activationDate = callerInfo.customer.activationDate;
        }
        if(callerInfo.customer.subscriptionType){
            updateData.subscriptionType = callerInfo.customer.subscriptionType;
        }
    }

    phones.findByIdAndUpdate(id, updateData,(err, res)=>{
        if(err) console.log(err);
        // else if(res) console.log(res);
        setTimeout(() => {
            parsePhoneRecord();
        }, 3000);
    })
}


parsePhoneRecord();
